package com.academiamoviles.batallapokemon

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var pokemonPikachu = Pokemon(nombre = "Pikachu")
    private var pokemonJiggly = Pokemon(nombre = "Jigglypuff")

    private var turno: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        empezar.setOnClickListener {
            restaurarPokemones()
            bloquearUi()
        }

        rgOpciones.setOnCheckedChangeListener { _, checkedId ->
            turno = when (checkedId) {
                R.id.rbPikachu -> 1
                R.id.rbJiggly -> 2
                else -> 3
            }
        }

        siguiente.setOnClickListener {
            when (turno) {
                1 -> atacar(pokemonPikachu, pokemonJiggly) // turno de Pikachu
                2 -> atacar(pokemonJiggly, pokemonPikachu) // turno de Jigglypuff
            }
        }

    }

    private fun atacar(pokemonAtaca: Pokemon, pokemonDefiende: Pokemon) {
        pokemonDefiende.vida -= pokemonAtaca.poderAtaque
        if (turno == 1) {
            vidaJiggly.text = getString(R.string.vida_x, pokemonDefiende.vida)
        } else {
            vidaPikachu.text = getString(R.string.vida_x, pokemonDefiende.vida)
        }
        if (pokemonDefiende.vida <= 0) {
            mostrarGanador(pokemonAtaca)
            restaurarUi()
        } else {
            if (turno == 1) {
                rgOpciones.check(R.id.rbJiggly)
            } else {
                rgOpciones.check(R.id.rbPikachu)
            }
        }
    }

    private fun restaurarPokemones() {
        pokemonPikachu.apply {
            vida = 100
            poderAtaque = poderPikachu.text.toString().toInt()
        }
        pokemonJiggly.apply {
            vida = 100
            poderAtaque = poderJiggly.text.toString().toInt()
        }
    }

    private fun bloquearUi() {
        empezar.isEnabled = false
        siguiente.isEnabled = true
        rbPikachu.isEnabled = false
        rbJiggly.isEnabled = false
        poderPikachu.isEnabled = false
        poderJiggly.isEnabled = false
    }

    private fun restaurarUi() {
        empezar.isEnabled = true
        siguiente.isEnabled = false
        rbPikachu.isEnabled = true
        rbJiggly.isEnabled = true
        poderPikachu.isEnabled = true
        poderJiggly.isEnabled = true

        vidaPikachu.text = getString(R.string.vida_default)
        vidaJiggly.text = getString(R.string.vida_default)

        rbPikachu.isChecked = true
    }

}


fun Activity.mostrarGanador(pokemon: Pokemon) {
    val builder = MaterialAlertDialogBuilder(this).apply {
        setMessage(getString(R.string.ganador_x, pokemon.nombre))
        setPositiveButton(R.string.volver_a_jugar, null)
    }
    builder.show()
}