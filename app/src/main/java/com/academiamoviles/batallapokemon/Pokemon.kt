package com.academiamoviles.batallapokemon

data class Pokemon(
    var nombre: String = "",
    var vida: Int = 100,
    var poderAtaque: Int = 0
)